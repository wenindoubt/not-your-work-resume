import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../header/header.component';
import { BodyComponent } from '../body/body.component';
import { ExperienceComponent } from '../body/experience/experience.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';

const COMPONENTS = [HeaderComponent, BodyComponent, ExperienceComponent];
const MAT_MODULES = [CommonModule, MatToolbarModule, MatListModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MAT_MODULES],
  exports: [...MAT_MODULES, ...COMPONENTS]
})
export class SharedModule {}
